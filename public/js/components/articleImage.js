const articleImage = () => {
	if (location.pathname.includes('article/')) {
		const article = document.querySelector('.article__entry'),
					images = article.getElementsByTagName('img');

		for(let i=0; i < images.length; i++) {
			if (images[i].naturalWidth < images[i].naturalHeight) {
				const imgOddParent = images[i].parentNode;
				const imgEvenParent = images[i+1].parentNode;

				imgOddParent.classList.add('img--float-left');
				if (imgEvenParent.classList.contains('img--float-left')) {
					imgEvenParent.classList.remove('img--float-left').classList.add('img--float-right');
				}
			}
		}
	}

	if (location.pathname.includes('article/')) {
		const article = document.querySelector('.article__entry'),
					links = article.getElementsByTagName('a');

		for(let i=0; i < links.length; i++) {
			links[i].setAttribute('target', '_blank');
		}
	}
};

export default articleImage;