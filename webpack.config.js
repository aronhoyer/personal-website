const path = require('path'),
			webpack = require('webpack'),
			MiniCssExtractPlugin = require('mini-css-extract-plugin');

require('dotenv').config();

const env = process.env.NODE_ENV;

module.exports = {
	mode: env,
	entry: {
		main: './public/js/main.js'
	},
	output: {
		path: path.resolve(__dirname, 'public'),
		filename: '[name].js'
	},
	devtool: 'source-map',
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /node_modules/,
				loader: 'babel-loader',
				options: {
					presets: ['env']
				}
			},
			{
				test: /\.sass$/,
				use: [
					MiniCssExtractPlugin.loader,
					{loader: 'css-loader', options: {importLoaders: 1}},
					'postcss-loader',
					'sass-loader'
				]
			},
			{
				test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
				use: [{
					loader: 'file-loader',
					options: {
						name: '[name].[ext]',
						outputPath: '/fonts'
					}
				}]
			}
		]
	},
	plugins: [
		new MiniCssExtractPlugin({filename: '[name].css'})
	]
};