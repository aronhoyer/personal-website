const express = require('express'),
			router = express.Router(),
			middleware = require('../controllers/middleware'),
			pageControllers = require('../controllers/page-controllers');

router.all('*', middleware.getNav)

router.get('/',
	middleware.getFeaturedProjects,
	middleware.getMediumPosts,
	pageControllers.homePage);

router.get('/about/:slug',
	middleware.getAboutPages,
	pageControllers.about);

router.get('/projects/:slug',
	middleware.getProjects,
	pageControllers.projects);

router.get('/project/:slug',
	// middleware.butterPreview,
	middleware.getProjectBySlug,
	pageControllers.project);

router.get('/articles',
	middleware.getMediumPosts,
	pageControllers.articles);

router.get('/article/:slug',
	middleware.getMediumPosts,
	pageControllers.article);

router.get('/contact',
	pageControllers.contact);

router.post('/contact/send',
	middleware.sendContactContent,
	pageControllers.contact);

module.exports = router;
