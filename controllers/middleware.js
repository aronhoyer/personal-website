const butter = require('buttercms')(process.env.BUTTER_KEY),
			nodemailer = require('nodemailer'),
			Parser = require('rss-parser');

const parser = new Parser({
				customFields: {
					item: [
						['content:encoded', 'body']
					]
				}
			});

// Get navigation elements from Butter CMS
// These are defined as content fields in the CMS
exports.getNav = (req, res, next) => {
	req.nav = [];

	butter.content.retrieve(['header_navigation']).then(resp => {
		req.nav.push(...resp.data.data.header_navigation);
		return next();
	}).catch(resp => {
		console.log(resp);
		return next();
	});
};

// Get Medium articles from the Medium RSS
exports.getMediumPosts = async (req, res, next) => {
	req.articles = [];

	// Parse the Medium feed through the rss-parser package
	const feed = await parser.parseURL('https://medium.com/feed/@aronhoyer');

	/*
		Make a cleaner article object literal so we can easily access
		the different parts of it without ugly loops and regexs
	*/
	feed.items.forEach(item => {
		const article = {
			title: item.title,
			slug: item.title.replace(/\s/g, '-').replace(/[^\w-]/g, '').toLowerCase(),
			link: item.guid,
			feature_image: item.body.split('<figure>')[1].split('<figcaption>')[0].split('src="')[1].split('" />')[0],
			lead: item.body.split('<strong>')[1].split('</strong>')[0],
			body: item.body.split(/\<\/figure>(.+)/)[1]
		};

		// Push the article object literal to the req.article array
		req.articles.push(article);
	});

	return next();
};

// Get case studies from Butter CMS
exports.getProjects = (req, res, next) => {
	req.projects = [];

	butter.page.list('project').then(resp => {
		/*
			Since Butter response gives an array of data,
			we need to spread the response into the req.projects array,
			making it an array of objects. Precisely what we want. 😍
		*/
		req.projects.push(...resp.data.data);
		return next();
	}).catch(resp => {
		console.log(resp);
		return next();
	});
};

exports.getFeaturedProjects = (req, res, next) => {
	req.featuredProjects = [];
	butter.page.list('project').then(resp => {
		const projects = resp.data.data;

		projects.map(project => {
			if (project.fields.featured) {req.featuredProjects.push(project)}
		});

		return next();
	}).catch(resp => {
		console.log(resp);
		return next();
	});
};

exports.getProjectBySlug = (req, res, next) => {
	req.project;

	butter.page.retrieve('project', req.params.slug).then(resp => {
		req.project = resp.data.data.fields;
		return next();
	}).catch(resp => {
		console.log(resp);
		return next();
	});
};

exports.butterPreview = (req, res, next) => {
	req.project;
	
	butter.page.retrieve('project&preview=1', req.query.preview).then(resp => {
		req.project = reps.data.data.fields;
	}).catch(resp => {
		console.log(resp);
	});

	return next();
}

exports.getAboutPages = (req, res, next) => {
	req.pages;

	butter.page.retrieve('about', req.params.slug).then(resp => {
		req.pages = resp.data.data.fields;
		return next();
	}).catch(resp => {
		console.log(resp);
		return next();
	});
};

exports.sendContactContent = (req, res, next) => {
	const formOutput = `
		<p><strong>${req.body.name}</strong> sent you this:</p>
		<p>${req.body.message}</p>
	`;

	const transporter = nodemailer.createTransport({
		host: `${process.env.MAIL_HOST}`,
		port: process.env.MAIL_PORT,
		secure: false,
		auth: {
			user: `${process.env.MAIL_USER}`,
			pass: `${process.env.MAIL_PWD}`
		}
	});

	const mailOptions = {
		from: `"${req.body.name}" <${req.body.email}>`,
		to: `${process.env.MAIL_USER}`,
		subject: 'A connection from the cloud',
		html: formOutput
	};

	transporter.sendMail(mailOptions, (err, info) => {
		if (err) {
			console.log(err);
			req.success = false;
			return next();
		}

		console.log('Message sent: %s', info.messageId);
		console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));

		req.success = true;

		return next();
	});
};
