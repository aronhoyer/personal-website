const butter = require('buttercms')(process.env.BUTTER_KEY);

exports.homePage = (req, res) => {
	req.randProj;

	req.featuredProjects.map(() => {
		req.randProj = req.featuredProjects[Math.floor(Math.random()*req.featuredProjects.length)];
	});

	res.render('homePage', {
		nav: req.nav,
		randProj: req.randProj,
		featProjects: req.featuredProjects.reverse(),
		articles: req.articles.reverse()
	});
};

exports.about = (req, res) => {
	res.render('about', {
		title: 'About',
		nav: req.nav,
		subNav: [
			{slug: 'what', title: 'What'},
			{slug: 'how', title: 'How'},
			{slug: 'why', title: 'Why'}
		],
		content: req.pages
	});
};

exports.projects = (req, res) => {
	res.render('projects', {
		title: 'Projects',
		nav: req.nav,
		subNav: [
			{slug: '/projects/clients', title: 'Clients'},
			{slug: '/projects/personal', title: 'Personal'}
		],
		projects: req.projects.reverse(),
	});
};

exports.project = (req, res) => {
	res.render(`project/${req.params.slug}`, {
		title: req.project.seo_title,
		nav: req.nav,
		project: req.project
	});
};

exports.articles = (req, res) => {
	res.render('articles', {
		title: 'Articles',
		nav: req.nav,
		articles: req.articles.reverse()
	});
};

exports.article = (req, res) => {
	const slug = req.params.slug,
				articles = req.articles;

	articles.forEach(article => {
		if (slug === article.slug) {
			res.render('article', {
				title: article.title,
				nav: req.nav,
				article
			});
		}
	});
};

exports.contact = (req, res) => {
	res.render('contact', {
		title: 'Contact',
		nav: req.nav,
		success: req.success
	});
};
