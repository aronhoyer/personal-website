require('dotenv').config();

const express = require('express'),
			bodyParser = require('body-parser'),
			http = require('http'),
			enforce = require('express-sslify'),
			routes = require('./routes/index.js'),
			pug = require('pug');

const app = express();

app.set('view engine', 'pug');

if (process.env.NODE_ENV === 'production') {
	app.use(enforce.HTTPS({ trustProtoHeader: true }));
}

app.use(express.static('./public'));

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());


app.use((req, res, next) => {
	res.locals.currentPath = req.path;
	res.locals.moment = require('moment');
	res.locals.social = [
		{slug: 'https://bitbucket.org/aronhoyer', title: 'Bitbucket'},
		{slug: 'https://behance.net/aronhoyer', title: 'Behance'},
		{slug: 'https://instagram.com/aronhoyer', title: 'Instagram'}
	];

	return next();
});

app.use('/', routes);

app.set('port', process.env.PORT || 3000);
http.createServer(app).listen(app.get('port'), function () {
	console.log(`Running on port ${this.address().port}...`);
});
